import Main from '@/components/main'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '首页',
          notCache: true,
          icon: 'md-home'
        },
        component: () => import('@/view/single-page/home')
      }
    ]
  },
  {
    path: '/system',
    name: 'system',
    meta: {
      icon: 'md-settings',
      title: '系统管理',
      access: ['system' ]
    },
    component: Main,
    children: [

      {
        path: '/systemconfig',
        name: 'systemconfig',
        meta: {
          icon: 'md-settings',
          title: '系统设置',
          access: ['system']
        },
        component: resolve => require(['@/view/system/systemconfig.vue'], resolve)
      },
      {
        path: '/printtag',
        name: 'printtag',
        meta: {
          icon: 'md-print',
          title: '打印标签',
          access: ['system']
        },
        component: resolve => require(['@/view/system/printtag.vue'], resolve)
      }
    ]
  },
  {
    path: '/organ',
    name: 'organ',
    meta: {
      icon: 'md-settings',
      title: '组织机构',
      access: ['role', 'auth', 'employee']
    },
    component: Main,
    children: [
      {
        path: '/auth',
        name: 'auth',
        meta: {
          icon: 'ios-paper',
          title: '权限管理',
          access: ['auth']
        },
        component: resolve => require(['@/view/system/auth.vue'], resolve)
      },
      {
        path: '/role',
        name: 'role',
        meta: {
          icon: 'md-document',
          title: '角色管理',
          access: ['role']
        },
        component: resolve => require(['@/view/system/role.vue'], resolve)
      }, {
        path: '/employee',
        name: 'employee',
        meta: {
          icon: 'md-people',
          title: '员工管理',
          access: ['employee']
        },
        component: resolve => require(['@/view/system/employee.vue'], resolve)
      }

    ]
  },

  {
    path: '/saleorderfolder',
    name: 'saleorderfolder',
    meta: {
      icon: 'md-folder',
      title: '销售管理',
      access: ['saleorder', 'saleorderback']
    },
    component: Main,
    children: [
      {
        path: '/saleorder',
        name: 'saleorder',
        meta: {
          icon: 'md-list-box',
          title: '销售单',
          access: ['saleorder']
        },
        component: resolve => require(['@/view/saleorder/saleorder.vue'], resolve)
      },
      {
        path: '/saleorderback',
        name: 'saleorderback',
        meta: {
          icon: 'md-list-box',
          title: '退货单',
          access: ['saleorderback']
        },
        component: resolve => require(['@/view/saleorderback/saleorderback.vue'], resolve)
      }
    ]
  },
  {
    path: '/purchaseorderfolder',
    name: 'purchaseorderfolder',
    meta: {
      icon: 'md-folder',
      title: '采购管理',
      access: ['purchaseorder', 'purchaseorderback']
    },
    component: Main,
    children: [
      {
        path: '/purchaseorder',
        name: 'purchaseorder',
        meta: {
          icon: 'md-list-box',
          title: '采购单',
          access: ['purchaseorder']
        },
        component: resolve => require(['@/view/purchaseorder/purchaseorder.vue'], resolve)
      },
      {
        path: '/purchaseorderback',
        name: 'purchaseorderback',
        meta: {
          icon: 'md-list-box',
          title: '采购退货单',
          access: ['purchaseorderback']
        },
        component: resolve => require(['@/view/purchaseorderback/purchaseorderback.vue'], resolve)
      }
    ]
  },
  {
    path: '/customerfolder',
    name: 'customerfolder',
    meta: {
      icon: 'md-folder',
      title: '客商管理',
      access: ['supplier', 'customer']
    },
    component: Main,
    children: [
      {
        path: '/customer',
        name: 'customer',
        meta: {
          icon: 'md-list-box',
          title: '客户管理',
          access: ['customer']
        },
        component: resolve => require(['@/view/customer/customer.vue'], resolve)
      },
      {
        path: '/supplier',
        name: 'supplier',
        meta: {
          icon: 'md-list-box',
          title: '供应商管理',
          access: ['supplier']
        },
        component: resolve => require(['@/view/supplier/supplier.vue'], resolve)
      }
    ]
  },
  {
    path: '/productfolder',
    name: 'productfolder',
    meta: {
      icon: 'md-folder',
      title: '商品管理',
      access: ['product']
    },
    component: Main,
    children: [
      {
        path: '/producttype',
        name: 'producttype',
        meta: {
          icon: 'md-list-box',
          title: '商品分类',
          access: ['product']
        },
        component: resolve => require(['@/view/product/producttype.vue'], resolve)
      },
      {
        path: '/product',
        name: 'product',
        meta: {
          icon: 'md-list-box',
          title: '商品管理',
          access: ['product']
        },
        component: resolve => require(['@/view/product/product.vue'], resolve)
      },
      {
        path: '/unit',
        name: 'unit',
        meta: {
          icon: 'md-list-box',
          title: '计量单位',
          access: ['product']
        },
        component: resolve => require(['@/view/product/unit.vue'], resolve)
      }

    ]
  },

  {
    path: '/storage',
    name: 'storage',
    meta: {
      icon: 'md-cube',
      title: '库存管理',
      access: ['productcheck', 'productsearch']
    },
    component: Main,
    children: [

      {
        path: '/productsearch',
        name: 'productsearch',
        meta: {
          icon: 'md-list-box',
          title: '产品库存',
          access: ['productsearch']
        },
        component: resolve => require(['@/view/storage/productsearch.vue'], resolve)
      },

      {
        path: '/productchecksave',
        name: 'productchecksave',
        meta: {
          icon: 'md-list-box',
          title: '产品盘点',
          access: ['productcheck']
        },
        component: resolve => require(['@/view/storage/productchecksave.vue'], resolve)
      }
    ]
  },
  {
    path: '/sta',
    name: 'sta',
    meta: {
      icon: 'md-podium',
      title: '财务报表',
      access: ['finance']
    },
    component: Main,
    children: [

      {
        path: '/finance',
        name: 'finance',
        meta: {
          icon: 'md-list-box',
          title: '收支费用',
          access: ['finance']
        },
        component: resolve => require(['@/view/finance/finance.vue'], resolve)
      }
    ]
  },
  {
    path: '/updatepwd',
    name: 'updatepwd',
    meta: {
      hideInMenu: true,
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: '/updatepwd',
        name: 'updatepwd',
        meta: {
          icon: 'ios-unlock',
          title: '修改密码'
        },
        component: resolve => require(['@/view/updatepwd/updatepwd.vue'], resolve)
      }
    ]
  },

  {
    path: '/refresh',
    name: 'refresh',
    meta: {
      hideInMenu: true,
      hideInBread: true
    },
    component: resolve => require(['@/view/refresh/refresh.vue'], resolve)
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  }
]
