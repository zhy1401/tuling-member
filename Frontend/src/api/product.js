import { axios } from '@/libs/api.request'

// #region  商品分类
export const SearchProductType = form => {
  return axios.post('api/Product/SearchProductType', form)
}

export const SaveProductType = form => {
  return axios.post('api/Product/SaveProductType', form)
}

export const DeleteProductType = id => {
  return axios.post('api/Product/DeleteProductType/' + id)
}
export const GetTreeProductType = () => {
  return axios.get('api/Product/GetTreeProductType')
}
// #endregion

// #region  商品
export const SearchProduct = form => {
  return axios.post('api/Product/SearchProduct', form)
}
export const SearchProductLog = form => {
  return axios.post('api/Product/SearchProductLog', form)
}

export const SaveProduct = form => {
  return axios.post('api/Product/SaveProduct', form)
}

export const DeleteProduct = id => {
  return axios.post('api/Product/DeleteProduct/' + id)
}
export const SaveProductCheck = form => {
  return axios.post('api/Product/SaveProductCheck', form)
}

// #endregion
// #region  计量单位
export const SearchUnit = form => {
  return axios.post('api/Product/SearchUnit', form)
}

export const SaveUnit = form => {
  return axios.post('api/Product/SaveUnit', form)
}

export const DeleteUnit = id => {
  return axios.post('api/Product/DeleteUnit/' + id)
}

// #endregion
