import { axios } from '@/libs/api.request'

// #region  销售单
export const SearchSaleOrderBack = form => {
  return axios.post('api/SaleOrderBack/SearchSaleOrderBack', form)
}

export const SaveSaleOrderBack = form => {
  return axios.post('api/SaleOrderBack/SaveSaleOrderBack', form)
}
export const GetSaleOrderBack = id => {
  return axios.get('api/SaleOrderBack/GetSaleOrderBack/' + id)
}
export const GetSaleOrderBackAndPrint = id => {
  return axios.get('api/SaleOrderBack/GetSaleOrderBackAndPrint/' + id)
}

export const DeleteSaleOrderBack = id => {
  return axios.post('api/SaleOrderBack/DeleteSaleOrderBack/' + id)
}
export const CheckSaleOrderBack = form => {
  return axios.post('api/SaleOrderBack/CheckSaleOrderBack', form)
}
