import { axios } from '@/libs/api.request'

// #region  销售单
export const SearchPurchaseOrderBack = form => {
  return axios.post('api/PurchaseOrderBack/SearchPurchaseOrderBack', form)
}

export const SavePurchaseOrderBack = form => {
  return axios.post('api/PurchaseOrderBack/SavePurchaseOrderBack', form)
}
export const GetPurchaseOrderBack = id => {
  return axios.get('api/PurchaseOrderBack/GetPurchaseOrderBack/' + id)
}
export const GetPurchaseOrderBackAndPrint = id => {
  return axios.get('api/PurchaseOrderBack/GetPurchaseOrderBackAndPrint/' + id)
}

export const DeletePurchaseOrderBack = id => {
  return axios.post('api/PurchaseOrderBack/DeletePurchaseOrderBack/' + id)
}
export const CheckPurchaseOrderBack = form => {
  return axios.post('api/PurchaseOrderBack/CheckPurchaseOrderBack', form)
}
