﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TulingMember.Core
{
    /// <summary>
    /// 。
    /// </summary> 
    public class cts_ProductLog: DEntityTenant
    {
      
        /// <summary>
        /// 。
        /// </summary>
     
        public long ProductId { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string ProductName { get; set; }


        /// <summary>
        /// 。
        /// </summary>

        public decimal OldNum { get; set; }


        /// <summary>
        /// 。
        /// </summary>

        public decimal ChangeNum { get; set; }


        /// <summary>
        /// 。
        /// </summary>

        public decimal NewNum { get; set; }

        /// <summary>
        /// 。
        /// </summary>

        public decimal OldWeight { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public decimal ChangeWeight { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public decimal NewWeight { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public ProductChangeTag TagType { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public string TagName { get; set; }

    
        /// <summary>
        /// 。
        /// </summary>
     
        public long RelationId { get; set; }

    
        

    }
}