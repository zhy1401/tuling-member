﻿namespace TulingMember.Core
{
    public class ClaimConst
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public const string CLAINM_USERID = "UserId";

        /// <summary>
        /// 账号
        /// </summary>
        public const string CLAINM_ACCOUNT = "Account";

        /// <summary>
        /// 名称
        /// </summary>
        public const string CLAINM_NAME = "Name";
        /// <summary>
        /// 类型
        /// </summary>
        public const string USER_TYPE = "UserType";
        /// <summary>
        /// 租户Id
        /// </summary>
        public const string TENANT_ID = "TenantId";

    }
}
