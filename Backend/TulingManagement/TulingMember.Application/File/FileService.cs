﻿using Furion.DatabaseAccessor;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using TulingMember.Core; 
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;   
using Serilog;
using Mapster;
using System.Threading.Tasks;
using Furion.DataEncryption;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Furion;
using Yitter.IdGenerator;

namespace TulingMember.Application
{


    public class FileService : IDynamicApiController
    {
        private readonly ILogger _logger;
        public FileService(ILogger logger)           
        {
            _logger = logger;          
        }
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="files"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        [HttpPost]
        public List<string> UploadFile(List<IFormFile> files,string dic= "")
        {
            if (string.IsNullOrEmpty(dic))
            {
                dic = "publicfile";
            }
            // 保存到网站根目录下的 uploads 目录
            var currentPath = App.WebHostEnvironment.WebRootPath;
            //文件夹路径
            var dicPath = $@"uploads\{dic}";
            var savePath = Path.Combine(currentPath, dicPath);
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            long size = files.Sum(f => f.Length);
            List<string> paths = new List<string>();
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    // 避免文件名重复，采用 雪花Id 生成
                    var filename = YitIdHelper.NextId().ToString() + Path.GetExtension(formFile.FileName);
                    var filePath = Path.Combine(savePath,filename);
                    var linkPath = dicPath.Replace(@"\", @"/") + "/" + filename;
                    using (var stream = File.Create(filePath))
                    {
                         formFile.CopyToAsync(stream);
                    }
                    paths.Add(linkPath);
                }
            }
            return paths;
        }

        /// <summary>
        /// 富文本上传文件
        /// </summary>
        /// <param name="file"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        [HttpPost]
        
        public string EditorUploadFile(IFormFile file, [FromForm] string dic = "")
        {
            if (string.IsNullOrEmpty(dic))
            {
                dic = "publicfile";
            }
            // 保存到网站根目录下的 uploads 目录
            var currentPath = App.WebHostEnvironment.WebRootPath;
            //文件夹路径
            var dicPath = $@"uploads\{dic}";
            var savePath = Path.Combine(currentPath, dicPath);
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            // 避免文件名重复，采用 雪花Id 生成
            var filename = YitIdHelper.NextId().ToString() + Path.GetExtension(file.FileName);
            var filePath = Path.Combine(savePath, filename);
            var linkPath = dicPath.Replace(@"\", @"/") + "/" + filename;
            using (var stream = File.Create(filePath))
            {
                file.CopyTo(stream);
            }
            return linkPath;
        }

    }
}
