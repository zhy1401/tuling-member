﻿using Furion.DatabaseAccessor;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using TulingMember.Core; 
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;   
using Serilog;
using Mapster;
using Furion.JsonSerialization;
using System.Threading.Tasks;
using Furion;
using StackExchange.Profiling.Internal;
using EFCore.BulkExtensions;

namespace TulingMember.Application.System
{


    public class SystemService : IDynamicApiController
    {
        private readonly ILogger _logger;
        private readonly IRepository<cts_SystemConfig> _configRepository;
        private readonly IRepository<cts_PrintTag> _printtagRepository; 
        public SystemService(ILogger logger
            , IRepository<cts_SystemConfig> configRepository
            , IRepository<cts_PrintTag> printtagRepository )           
        {
            _logger = logger;
            _configRepository = configRepository;
            _printtagRepository = printtagRepository; 
        }
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public cts_SystemConfig GetSystemConfig() {
            var config= _configRepository.FirstOrDefault();
            if (config == null)
            { 
                config = new cts_SystemConfig();
            }
            return config;
        }
        /// <summary>
        /// 保存系统配置
        /// </summary>
        /// <param name="config"></param>
        public void SaveSystemConfig(cts_SystemConfig config)
        {
           var oldConfig = _configRepository.DetachedEntities.FirstOrDefault();
            if (oldConfig==null)
            { 
                _configRepository.Insert(config);
            } 
            else {
                config.Id = oldConfig.Id; 
                _configRepository.Update(config);
            }
            var tenantid = App.User.FindFirst(ClaimConst.TENANT_ID).Value;
            var redisKey = AppStr.SystemConfig + "_" + tenantid;
            RedisHelper.SetString(redisKey, config.ToJson());
        }
        /// <summary>
        /// 获取打印标签
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<cts_PrintTag> GetPrintTag(int type)
        {
            var search = _printtagRepository.AsQueryable();
            if (type==1)
            {
                search=search.Where(m=>m.IsShow==type);
            }
            return _printtagRepository.AsQueryable().OrderBy(m=>m.SortNo).ToList();
        }
        /// <summary>
        /// 保存打印标签
        /// </summary>
        /// <param name="tag"></param>
        public void SavePrintTag(cts_PrintTag tag)
        {
             
            if (tag.Id == 0)
            {
                _printtagRepository.Insert(tag);
            }
            else
            {
                _printtagRepository.Update(tag);
            }
        }
        /// <summary>
        /// 删除打印标签
        /// </summary>
        /// <param name="id"></param>
        public void DeletePrintTag(long id)
        {

            _printtagRepository.FakeDelete(id);
        }
 

        /// <summary>
        /// 保存前端错误
        /// </summary>
        [AllowAnonymous]
        public bool SaveErrorLogger(dynamic info)
        { 
            _logger.Error(JSON.Serialize(info));
            return true;
        }
        
    }
}
