﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TulingMember.Web.Entry.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            ViewBag.Description = "TulingMember | 规范化接口";

            return View();
        }
        
    }
}